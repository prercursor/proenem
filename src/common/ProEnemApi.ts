import axios, { AxiosRequestConfig } from 'axios'

const endpoint = 'https://dev.api.prodigioeducacao.com/v1'

class ProEnemApi {
  private _throwError(error: any) {
    throw new Error(`AdApi error, ${error}`)
  }

  private getAxiosConfig(accessToken: string = null): AxiosRequestConfig {
    const headers = {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      ...(accessToken ? { Authorization: 'Bearer ' + accessToken } : {}),
    }

    return {
      baseURL: endpoint,
      timeout: 2000,
      headers,
    }
  }

  private get(route: string, accessToken?: string) {
    if (accessToken) {
      const config = this.getAxiosConfig(accessToken)
      return axios.get(route, config)
    }

    return axios.get(endpoint + route)
  }

  private post(route: string, params = {}, accessToken?: string) {
    if (accessToken) {
      const config = this.getAxiosConfig(accessToken)
      return axios.post(route, params, config)
    }

    return axios.post(endpoint + route, params)
  }

  async signIn(email: string, password: string) {
    const resp = await this.post('/token', { email, password })

    if (resp.status !== 200) {
      this._throwError(`request failed with status ${resp.status}`)
    }

    return resp.data
  }

  async getMe(token: string) {
    const resp = await this.get('/person/me', token)

    if (resp.status !== 200) {
      this._throwError(`request failed with status ${resp.status}`)
    }

    return resp.data
  }
}

export default new ProEnemApi()
