import { combineReducers } from 'redux'
import { all, fork } from 'redux-saga/effects'

import { IAuthState, authSaga, AuthReducer } from './auth'

export interface IApplicationState {
  auth: IAuthState
}

export const rootReducer = combineReducers<IApplicationState>({
  auth: AuthReducer,
})

export function* rootSaga() {
  yield all([fork(authSaga)])
}
