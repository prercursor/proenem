import { createStore, applyMiddleware } from 'redux'
import { rootReducer, rootSaga } from './root'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

const persistConfig = {
  key: 'root',
  storage,
}
const persistedReducer = persistReducer(persistConfig, rootReducer)

const sagaMiddleware = createSagaMiddleware()

let store = createStore(
  persistedReducer,
  applyMiddleware(sagaMiddleware, logger)
)
export default store
export let persistor = persistStore(store)

sagaMiddleware.run(rootSaga)
