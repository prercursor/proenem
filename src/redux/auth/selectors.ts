import { IApplicationState } from 'src/redux/root'

export const selectors = {
  getUser: (state: IApplicationState) => state.auth.user || {},
  isSignedIn: (state: IApplicationState) => state.auth.user && state.auth.token,
}