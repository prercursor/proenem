export interface IUser {
  readonly name: string
  readonly email: string
  readonly avatarUrl: string
  readonly id: string
}

export interface IAuthState {
  readonly user: IUser
  readonly token: string
  readonly signInRequested: boolean
  readonly signInFailMsg: string
}

export enum ActionTypes {
  SIGN_IN_REQUESTED = 'pro-enem/auth/SIGN_IN_REQUESTED',
  SIGN_IN_SUCCESS = 'pro-enem/auth/SIGN_IN_SUCCESS',
  SIGN_IN_FAIL = 'pro-enem/auth/SIGN_IN_FAIL',
  SIGN_OUT_REQUESTED = 'pro-enem/auth/SIGN_OUT_REQUESTED',
}
