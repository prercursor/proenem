import { all, fork, put, takeLatest } from 'redux-saga/effects'

import ProEnemApi from 'src/common/ProEnemApi'
import { ActionTypes as types, IUser } from './types'
import { actions } from './actions'

function* signIn({ payload }) {
  try {
    const signInResp = yield ProEnemApi.signIn(payload.email, payload.password)
    const getMeResp = yield ProEnemApi.getMe(signInResp.token)
    const user: IUser = {
      avatarUrl: getMeResp.imageProfile,
      email: getMeResp.email,
      name: getMeResp.name,
      id: getMeResp.id,
    }

    yield put(actions.signInSuccess(user, signInResp.token))
  } catch (err) {
    yield put(actions.signInFail(err.message))
  }
}

function* watcher() {
  yield takeLatest(types.SIGN_IN_REQUESTED, signIn)
}

export function* authSaga() {
  yield all([fork(watcher)])
}
