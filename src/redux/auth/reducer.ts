import * as fromAction from './actions'
import { ActionTypes as types, IAuthState } from './types'

const initialState: IAuthState = {
  user: null,
  token: null,
  signInRequested: false,
  signInFailMsg: null,
}

const reducer = (
  state = initialState,
  action: fromAction.Actions
): IAuthState => {
  switch (action.type) {
    case types.SIGN_IN_REQUESTED:
      return { ...state, signInRequested: true, signInFailMsg: null }

    case types.SIGN_IN_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        signInRequested: false,
        signInFailMsg: null,
      }

    case types.SIGN_IN_FAIL:
      return {
        ...state,
        signInRequested: false,
        signInFailMsg: 'Failed to sign in, please try again.',
      }

    case types.SIGN_OUT_REQUESTED:
      return { ...state, user: null, token: null }

    default:
      return state
  }
}

export { reducer as AuthReducer }
