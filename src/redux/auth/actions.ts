import { createAction } from 'src/common/actionHelper'
import { ActionsUnion } from 'src/common/typeHelper'

import { ActionTypes as types, IUser } from './types'

export const actions = {
  signIn: (email: string, password: string) =>
    createAction(types.SIGN_IN_REQUESTED, { email, password }),
  signInFail: (err: string) => createAction(types.SIGN_IN_FAIL, err),
  signInSuccess: (user: IUser, token: string) =>
    createAction(types.SIGN_IN_SUCCESS, { user, token }),
  signOut: () => createAction(types.SIGN_OUT_REQUESTED),
}

export type Actions = ActionsUnion<typeof actions>
