import * as React from 'react'
import { Alert } from 'react-native'
import { connect } from 'react-redux'
import { NavigationScreenProp, NavigationState } from 'react-navigation'

import WithLoader from 'src/common/WithLoader'
import util from 'src/common/util'
import * as authRedux from 'src/redux/auth'
import { IApplicationState } from 'src/redux/root'
import { SignInView } from './SignInView'

const SignInViewWithLoader = WithLoader(SignInView)
export interface IState {
  formIsValid: boolean
  email: string
  emailError: string
  password: string
  passwordError: string
}

export interface IReduxProps {
  signInFailMsg: string
  isSignedIn: boolean
  signInRequested: boolean
  signIn: (email: string, pass: string) => void
}

interface INavProps {
  navigation: NavigationScreenProp<NavigationState>
}

type Props = authRedux.Actions & IReduxProps & INavProps

class SignInScreen extends React.Component<Props, IState> {
  constructor(props: Props) {
    super(props)

    if (this.props.isSignedIn) this.props.navigation.navigate('profile')
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.signInFailMsg &&
      this.props.signInFailMsg != prevProps.signInFailMsg
    ) {
      Alert.alert('Error', this.props.signInFailMsg, [{ text: 'OK' }], {
        cancelable: false,
      })
    }

    if (this.props.isSignedIn) this.props.navigation.navigate('profile')
  }

  state: Readonly<IState> = {
    formIsValid: false,
    email: '',
    emailError: null,
    password: '',
    passwordError: null,
  }

  isFieldValueValid = (value: string) => {
    return !value || value.length == 0
  }

  validateForm = () => {
    let formIsValid = true
    let emailError: string = null
    let passwordError: string = null

    if (this.isFieldValueValid(this.state.email)) {
      emailError = 'This field is required'
      formIsValid = false
    } else if (!util.isEmailValid(this.state.email)) {
      emailError = 'Invalid email'
      formIsValid = false
    }

    if (this.isFieldValueValid(this.state.password)) {
      passwordError = 'This field is required'
      formIsValid = false
    }

    this.setState({ passwordError, emailError, formIsValid })
    return formIsValid
  }

  onChangeField = (fieldName: string, value: any) => {
    const newState = {}
    newState[fieldName] = value
    this.setState(newState)
    setTimeout(() => this.validateForm(), 0)
  }

  onPressSubmit = () => {
    if (!this.validateForm()) return

    this.props.signIn(this.state.email, this.state.password)
  }

  render() {
    return (
      <SignInViewWithLoader
        loading={this.props.signInRequested}
        email={this.state.email}
        emailError={this.state.emailError}
        password={this.state.password}
        passwordError={this.state.passwordError}
        onPressSubmit={this.onPressSubmit}
        onChangePassword={password => this.onChangeField('password', password)}
        onChangeEmail={email => this.onChangeField('email', email)}
      />
    )
  }
}

const mapStateToProps = (state: IApplicationState) => ({
  signInFailMsg: state.auth.signInFailMsg,
  user: authRedux.selectors.getUser(state),
  token: state.auth.token,
  isSignedIn: authRedux.selectors.isSignedIn(state),
  signInRequested: state.auth.signInRequested,
})

export default connect(
  mapStateToProps,
  { ...authRedux.actions }
)(SignInScreen)
