import { StyleSheet, ViewStyle, ImageStyle, TextStyle } from 'react-native'

interface IStyles {
  container: ViewStyle
  title: TextStyle
}

export default StyleSheet.create<IStyles>({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16
  },
  title: {
      fontSize: 24,
      marginBottom: 24
  }
})
