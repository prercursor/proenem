import * as React from 'react'
import { View, Text } from 'react-native'
import { Button } from 'src/components/Button'
import { TextInput } from 'src/components/TextInput'


import styles from './styles'

export interface Props {
  email: string
  emailError: string
  onChangeEmail: (text: string) => void
  password: string
  passwordError: string
  onChangePassword: (text: string) => void
  onPressSubmit: () => void
}

export class SignInView extends React.Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Pro Enem</Text>
        <TextInput
          placeholder="email"
          value={this.props.email}
          onChangeText={this.props.onChangeEmail}
          error={this.props.emailError}
        />

        <TextInput
          placeholder="password"
          value={this.props.password}
          onChangeText={this.props.onChangePassword}
          error={this.props.passwordError}
          secureTextEntry={true}
        />
        <Button text="Submit" onPress={this.props.onPressSubmit} />
      </View>
    )
  }
}


