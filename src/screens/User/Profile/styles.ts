import { StyleSheet, ViewStyle, ImageStyle, TextStyle } from 'react-native'

interface IStyles {
  container: ViewStyle
  label: TextStyle
  labelValue: TextStyle
  avatar: ImageStyle
}

export default StyleSheet.create<IStyles>({
  container: {
    flex: 1,
    padding: 16,
  },
  avatar: {
    width: '80%',
    height: 200,
    alignSelf: 'center',
    marginBottom: 24,
  },
  label: {
    fontSize: 14,
    paddingBottom: 8,
  },
  labelValue: {
    fontSize: 24,
    marginBottom: 24,
  },
})
