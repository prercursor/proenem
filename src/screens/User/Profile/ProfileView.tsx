import * as React from 'react'
import { View, Text, Image, ImageSourcePropType } from 'react-native'
import { Button } from 'src/components/Button'

import styles from './styles'

export interface IProps {
  avatarUrl: ImageSourcePropType
  name: string
  email: string
  onPressSignOut: () => void
}

export const UserProfileView = ({
  avatarUrl,
  name,
  email,
  onPressSignOut,
}: IProps) => (
  <View style={styles.container}>
    <Image source={avatarUrl} style={styles.avatar} resizeMode="contain" />
    <Text style={styles.label}>Name: {name}</Text>
    <Text style={styles.label}>Email: {email}</Text>

    <View style={{ marginTop: 100 }}>
      <Button text="Sign out" onPress={onPressSignOut} />
    </View>
  </View>
)
