import * as React from 'react'
import { NavigationScreenProp, NavigationState } from 'react-navigation'
import { connect } from 'react-redux'

import * as authRedux from 'src/redux/auth'
import { IApplicationState } from 'src/redux/root'
import { UserProfileView } from './ProfileView'

export interface IReduxProps {
  user: authRedux.IUser
  signOut: () => void
  isSignedIn: boolean
}
interface INavProps {
  navigation: NavigationScreenProp<NavigationState>
}

type Props = IReduxProps & authRedux.Actions & INavProps

export interface ProfileScreenState {}

class UserProfileScreen extends React.Component<Props, ProfileScreenState> {
  onPressSignOut = () => {
    this.props.signOut()
    this.props.navigation.navigate('SignIn')
  }

  render() {
    return (
      <UserProfileView
        onPressSignOut={this.onPressSignOut}
        name={this.props.user.name}
        email={this.props.user.email}
        avatarUrl={{ uri: this.props.user.avatarUrl }}
      />
    )
  }
}

const mapStateToProps = (state: IApplicationState) => ({
  user: authRedux.selectors.getUser(state),
  isSignedIn: authRedux.selectors.isSignedIn(state),
})

export default connect(
  mapStateToProps,
  { ...authRedux.actions }
)(UserProfileScreen)
