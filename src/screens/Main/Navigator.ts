import {
  createStackNavigator,
  createAppContainer,
  StackNavigatorConfig,
  NavigationStackScreenOptions,
  NavigationScreenConfig,
} from 'react-navigation'

import { SignInScreen } from 'src/screens/Auth/SignIn'
import { UserProfileScreen } from 'src/screens/User/Profile'
import R from 'src/res'

const commonStackNavOptions: StackNavigatorConfig = {
  headerLayoutPreset: 'center',
  headerMode: 'float',
  headerBackTitleVisible: false,
}

const commonNavOptions: NavigationScreenConfig<NavigationStackScreenOptions> = {
  headerTitleStyle: {
    fontWeight: 'bold',
    color: R.colors.LIGHT_TEXT,
  },
  headerStyle: {
    borderBottomWidth: 0,
    backgroundColor: R.colors.PRIMAY,
    elevation: 0,
  },
  headerTintColor: R.colors.LIGHT_TEXT,
}
const AppNavigator = createStackNavigator(
  {
    SignIn: SignInScreen,
    profile: UserProfileScreen
  },
  {
    ...commonStackNavOptions,
    initialRouteName: 'SignIn',
    defaultNavigationOptions: {
      ...commonNavOptions,
      headerLeft: null,
    },
  }
)

export default createAppContainer(AppNavigator)
