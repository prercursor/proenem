import React from 'react'
import { StatusBar } from 'react-native'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import store, { persistor } from 'src/redux/store'
import Navigator from './Navigator'

export class MainScreen extends React.Component {
  constructor(props: any) {
    super(props)

    StatusBar.setBarStyle('light-content')
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Navigator />
        </PersistGate>
      </Provider>
    )
  }
}
