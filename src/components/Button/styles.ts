import { StyleSheet, ViewStyle, TextStyle } from 'react-native'
import { forInStatement } from '@babel/types'

interface IStyles {
  dimension: ViewStyle
  gradient: ViewStyle
  buttonText: TextStyle
  secondaryBtn: ViewStyle
}

export default StyleSheet.create<IStyles>({
  dimension: {
    backgroundColor: '#395AFF',
    paddingVertical: 16,
    width: '100%',
    margin: 8,
    borderRadius: 8
  },
  gradient: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  secondaryBtn: {
    borderColor: '#fff',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
    borderRadius: 8,
    fontSize: 16,
  },
})
