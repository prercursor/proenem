import * as React from 'react'
import { Text, TouchableOpacity } from 'react-native'

import styles from './styles'

export interface Iprops {
  text: string
  onPress: () => void
}

export function Button({ text, onPress }: Iprops) {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={onPress}
      style={styles.dimension}
    >
      <Text style={styles.buttonText}>{text}</Text>
    </TouchableOpacity>
  )
}

//export default React.memo(Button)
