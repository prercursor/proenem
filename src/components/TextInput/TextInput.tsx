import * as React from 'react'
import {
  View,
  Text,
  TextInput as RNTextInput,
  TextInputProps,
} from 'react-native'

import styles from './styles'

export interface Iprops extends TextInputProps {
  error: string
  dirty?: boolean
}

const getContainerStyle = (hasError: boolean) => {
  if (hasError) return [styles.container, styles.inputError]

  return styles.container
}

export function TextInput(props: Iprops) {
  const { error } = props
  const hasError = error != null && error.length > 0

  return (
    <View style={getContainerStyle(hasError)}>
      <RNTextInput {...props} style={styles.input} />
      {hasError && <Text style={styles.errorMsg}>{error}</Text>}
    </View>
  )
}
