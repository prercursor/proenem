import { StyleSheet, ViewStyle, TextStyle } from 'react-native'

interface IStyles {
  container: ViewStyle
  input: TextStyle
  inputError: TextStyle
  errorMsg: TextStyle
}

export default StyleSheet.create<IStyles>({
  container: {
    width: '100%',
    margin: 8,
    minHeight: 48,
    justifyContent: 'center',
    paddingHorizontal: 8,
    borderRadius: 8,
    backgroundColor: '#F0F0F0',
  },
  input: {
    color: '#444',
    width: '100%'
  },
  inputError: {
    borderColor: '#FE5F5F',
    borderWidth: 1,
    
  },
  errorMsg: {
    marginTop: 8,
    color: '#FE5F5F',
    fontWeight: '600',
    fontSize: 11,
    position: 'absolute',
    bottom: 8,
    right: 8
  },
})
